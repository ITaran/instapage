package com.example.testappanoda.model.pojo

import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("commentAuthorName") val commentAuthorName: String,
    @SerializedName("commentText") val commentText: String
)
