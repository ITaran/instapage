package com.example.testappanoda.model.pojo

import com.example.testappanoda.model.pojo.Post
import com.google.gson.annotations.SerializedName

data class ProfilePost(@SerializedName("posts") val posts: List<Post>)