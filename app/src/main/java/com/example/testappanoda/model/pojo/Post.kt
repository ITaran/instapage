package com.example.testappanoda.model.pojo

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("userPhoto") val userPhoto: String,
    @SerializedName("userName") val userName: String,
    @SerializedName("userLocation") val userLocation: String,
    @SerializedName("photos") val photos: List<String>,
    @SerializedName("likesCount") val likesCount: Int,
    @SerializedName("likes") val likes: String,
    @SerializedName("comments") val comments: List<Comment>,
    @SerializedName("lastVisit") val lastVisit: String
)