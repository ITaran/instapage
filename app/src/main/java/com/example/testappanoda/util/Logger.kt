package com.example.testappanoda.util

import android.util.Log
import com.example.testappanoda.BuildConfig

fun v(msg: String) {
    if (BuildConfig.DEBUG) {
        Log.d("LogsApp", msg)
    }
}