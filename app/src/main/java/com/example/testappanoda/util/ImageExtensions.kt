package com.example.testappanoda.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.design.internal.BottomNavigationMenu
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget

fun ImageView.setImageWithCrop(url: String) {
    Glide.with(context)
        .load(url)
        .apply(RequestOptions().circleCrop())
        .into(this)
}

fun ImageView.setImage(url: String) {
    Glide.with(context)
        .load(url)
        .apply(RequestOptions().centerCrop())
        .into(this)
}