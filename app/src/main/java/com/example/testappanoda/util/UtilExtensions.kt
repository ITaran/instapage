package com.example.testappanoda.util

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.TextView
import com.example.testappanoda.R
import com.example.testappanoda.model.pojo.Comment
import com.example.testappanoda.model.pojo.ProfilePost
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter

fun getStringFromJson(context: Context): ProfilePost? {
    val inputStream = context.resources.openRawResource(R.raw.instagram_data)
    val writer = StringWriter()
    val buffer = CharArray(1024)
    inputStream.use {
        val reader = BufferedReader(InputStreamReader(it, "UTF-8"))
        var n = 0
        while (n != -1) {
            writer.write(buffer, 0, n)
            n = reader.read(buffer)
        }
    }
    val jsonString = writer.toString()
    return Gson().fromJson(jsonString, ProfilePost::class.java)
}

fun TextView.setLiked(usersList: String?, postLikedCount: Int?) {
    val text = String.format(context.getString(R.string.post_liked_by), "<b>${usersList!!}</b>", postLikedCount!!)
    this.text = htmlBuilder(text)
}

fun TextView.setComments(postsComments: List<Comment>?) {
    for (comment in postsComments!!) {
        val oneComment = "</b> ${comment.commentText}"
        this.text = htmlBuilder(oneComment)
    }
}

private fun htmlBuilder(text: String): Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
} else {
    Html.fromHtml(text)
}