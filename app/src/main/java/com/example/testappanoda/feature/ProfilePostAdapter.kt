package com.example.testappanoda.feature

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testappanoda.R
import com.example.testappanoda.model.pojo.Post
import com.example.testappanoda.util.setComments
import com.example.testappanoda.util.setImageWithCrop
import com.example.testappanoda.util.setLiked
import kotlinx.android.synthetic.main.profile_posts_item.view.*

class ProfilePostAdapter(
    private val posts: List<Post>, private val clickListener: PostClickListener
) : RecyclerView.Adapter<ProfilePostAdapter.ProfilePostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfilePostViewHolder =
        getProfilePostViewHolder(parent)

    override fun onBindViewHolder(holder: ProfilePostViewHolder, position: Int) {
        onBindFeedViewHolder(holder, position)
    }

    override fun getItemCount(): Int = posts.size

    private fun getProfilePostViewHolder(parent: ViewGroup) =
        ProfilePostViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.profile_posts_item, parent, false)
        )

    private fun onBindFeedViewHolder(holder: ProfilePostViewHolder, position: Int) {
        val post: Post = posts[position]
        val itemView: View = holder.itemView
        setView(itemView, post)
        setViewOnClickListeners(itemView, position)
        setViewPager(itemView, post)
    }

    private fun setView(itemView: View, post: Post) {
        itemView.ivUserPhoto.setImageWithCrop(post.userPhoto)
        itemView.tvUserName.text = post.userName
        itemView.tvUserLocation.text = post.userLocation
        itemView.tvLikes.setLiked(post.likes, post.likesCount)
        itemView.tvNameAuthor.text = post.userName
        itemView.tvComment.setComments(post.comments)
        itemView.tvLastVisit.text = post.lastVisit
    }

    private fun setViewOnClickListeners(itemView: View, position: Int) {
        itemView.ivSettings.setOnClickListener { clickListener.onSettingsClicked(it, position) }
        itemView.ivFavorite.setOnClickListener { clickListener.onLikeClicked(it, position) }
        itemView.ivComment.setOnClickListener { clickListener.onCommentClicked(it, position) }
        itemView.ivResend.setOnClickListener { clickListener.onResendClicked(it, position) }
        itemView.ivSavePost.setOnClickListener { clickListener.onSavePageClicked(it, position) }
        itemView.tvUserLocation.setOnClickListener { clickListener.onChangeLocationClicked(it, position) }
    }

    private fun setViewPager(itemView: View, post: Post) {
        val viewPager = itemView.viewpager
        viewPager.adapter = SliderAdapter(itemView.context, post.photos)
        itemView.indicator.setViewPager(viewPager)
    }

    class ProfilePostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface PostClickListener {
        fun onSettingsClicked(view: View, position: Int)

        fun onLikeClicked(view: View, position: Int)

        fun onCommentClicked(view: View, position: Int)

        fun onResendClicked(view: View, position: Int)

        fun onSavePageClicked(view: View, position: Int)

        fun onChangeLocationClicked(view: View, position: Int)
    }
}