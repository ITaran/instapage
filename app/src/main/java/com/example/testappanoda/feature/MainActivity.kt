package com.example.testappanoda.feature

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.testappanoda.R
import com.example.testappanoda.core.BaseActivity
import com.example.testappanoda.model.pojo.ProfilePost
import com.example.testappanoda.util.getStringFromJson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), ProfilePostAdapter.PostClickListener {

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> showToast(getString(R.string.home))
            R.id.navigation_dashboard -> showToast(getString(R.string.search))
            R.id.navigation_notifications -> showToast(getString(R.string.publishing))
            R.id.navigation_favorite -> showToast(getString(R.string.likes))
            R.id.navigation_profile -> showToast(getString(R.string.profile))
        }
        false
    }

    override fun obtainLayoutResId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val profilePost: ProfilePost? = getStringFromJson(this)
        initView()
        initRecyclerView(profilePost)
    }

    override fun onChangeLocationClicked(view: View, position: Int) {
        showToast(getString(R.string.location_selecting))
    }

    override fun onSettingsClicked(view: View, position: Int) {
        showToast(getString(R.string.settings))
    }

    override fun onLikeClicked(view: View, position: Int) {
        showToast(getString(R.string.like))
    }

    override fun onCommentClicked(view: View, position: Int) {
        showToast(getString(R.string.comment))
    }

    override fun onResendClicked(view: View, position: Int) {
        showToast(getString(R.string.send_on))
    }

    override fun onSavePageClicked(view: View, position: Int) {
        showToast(getString(R.string.save_post))
    }

    private fun initView() {
        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        ivMakePhoto.setOnClickListener { showToast(getString(R.string.make_photo)) }
        ivOpenMessage.setOnClickListener { showToast(getString(R.string.open_direct)) }
    }

    private fun initRecyclerView(profilePost: ProfilePost?) {
        val linearLayoutManager = LinearLayoutManager(this)
        rvProfilePost.layoutManager = linearLayoutManager
        val profilePostAdapter = ProfilePostAdapter(profilePost!!.posts, this)
        rvProfilePost.adapter = profilePostAdapter
    }
}