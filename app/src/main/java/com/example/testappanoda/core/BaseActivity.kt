package com.example.testappanoda.core

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    protected abstract fun obtainLayoutResId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(obtainLayoutResId())
    }

    override fun setContentView(layoutResID: Int) {
        throw UnsupportedOperationException("Use obtainLayoutResId()")
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}